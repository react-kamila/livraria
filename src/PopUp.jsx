import M from 'materialize-css';

const PopUp = {
    success(message) {
        M.toast({ html: message, classes: 'green', displayLength: 3000 });
    },
    error(message) {
        M.toast({ html: message, classes: 'red', displayLength: 3000 });
    },
    warn(message) {
        M.toast({ html: message, classes: 'orange', displayLength: 3000 });
    }

}

export default PopUp;