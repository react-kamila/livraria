import React from 'react';
import { NavLink } from 'react-router-dom';

const LinkWrapper = props => {
    return (
        /** High Order Component utilizado no Header.jsx
         * Passando todas as propriedades para o NavLink através do spreadOperator {...props}
         * Exemplo para propriedade "to": to={props.to}
         */
        <NavLink activeStyle={{fontWeight: "bold", backgroundColor: "rgba(0,0,0,0.1)"}} {...props} />
    );
}
export default LinkWrapper;