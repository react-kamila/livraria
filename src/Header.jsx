import React from 'react';
import './App.css';
import LinkWrapper from './LinkWrapper';

const Header = () => {
    return (
        <nav>
            <div className="nav-wrapper indigo lighten-1">
                <LinkWrapper to="/" className="brand-logo ml-10" activeStyle={{}}>Livraria da Kamila</LinkWrapper>
                <ul id="nav-mobile" className="right">
                    <li><LinkWrapper to="/autores">Autores</LinkWrapper></li>
                    <li><LinkWrapper to="/livros">Livros</LinkWrapper></li>
                    <li><LinkWrapper to="/sobre">Sobre</LinkWrapper></li>
                </ul>
            </div>
        </nav>

    )
}

export default Header;