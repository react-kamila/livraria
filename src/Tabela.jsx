import React, { Component } from 'react';

const TableBody = props => {

    const linhas = props.autores.map((autor, index) => {
        return (
            <tr key={index}>
                <td>{autor.nome}</td>
                <td>{autor.livro}</td>
                <td>{autor.preco}</td>
                <td><button onClick={() => { props.removeAutor(index)}} 
                        className="waves-effect waves-light btn indigo lighten-2">
                            Remover
                    </button>
                </td>
            </tr>
        )
    })
    return (
        <tbody>
            {linhas}
        </tbody>
    )
}
export default class Tabela extends Component {
    render() {

        const { autores, removeAutor } = this.props;
        return (
            <table className="centered highlight">
                <thead>
                    <tr>
                        <th>Autores</th>
                        <th>Livros</th>
                        <th>Preços</th>
                        <th>Remover</th>
                    </tr>
                </thead>
                <TableBody autores={autores} removeAutor={removeAutor} />
            </table>
        )
    }
}