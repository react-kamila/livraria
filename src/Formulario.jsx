import React, { Component } from 'react'
import FormValidator from './FormValidator';
import PopUp from './PopUp';

export default class Formulario extends Component {

    constructor(props) {
        super(props);

        this.validador = new FormValidator([
            {
                campo: 'nome',
                metodo: 'isEmpty',
                validoQuando: false,
                mensagem: 'Digite um nome.'
            },
            {
                campo: 'livro',
                metodo: 'isEmpty',
                validoQuando: false,
                mensagem: 'Digite um livro.'
            },
            {
                campo: 'preco',
                metodo: 'isInt',
                args: [{ min: 0, max: 99999 }],
                validoQuando: true,
                mensagem: 'Digite um preço válido'
            },
        ]);

        this.stateInicial = {
            nome: '',
            livro: '',
            preco: '',
            validacao: this.validador.objetoParaValidacao()
        }

        this.state = this.stateInicial;
    }

    listennerInput = event => {
        const { name, value } = event.target;
        this.setState({ [name]: value });
    }

    submitForm = () => {

        // Valida os campos do state
        const validacao = this.validador.valida(this.state);

        if (validacao.isValid) {
            // Executa função onSubmit recebida via props do App.js
            this.props.onSubmit(this.state);
            this.setState(this.stateInicial);
        } else {
            console.log('submit bloqueado');
            const { nome, livro, preco } = validacao;
            const campos = [nome, livro, preco];
            const camposInvalidos = campos.filter(elem => {
                return elem.isInvalid;
            });
            camposInvalidos.forEach(campo => {
                PopUp.error(campo.message);
            });
        }
    }

    render() {

        const { nome, livro, preco } = this.state;

        return (
            <form>

                <div className="row">

                    <div className="input-field col s4">
                        <input
                            className="validate"
                            id="nome" placeholder="Nome do autor"
                            type="text"
                            name="nome"
                            value={nome}
                            onChange={this.listennerInput}
                        />
                        <label className="active" htmlFor="nome">Nome</label>
                    </div>

                    <div className="input-field col s4">
                        <label className="active" htmlFor="livro">Livro</label>
                        <input
                            className="validate"
                            id="livro" placeholder="Livro"
                            type="text"
                            name="livro"
                            value={livro}
                            onChange={this.listennerInput}
                        />
                    </div>

                    <div className="input-field col s4">
                        <label className="active" htmlFor="preco">Preço</label>
                        <input
                            className="validate"
                            id="preco" placeholder="Preço"
                            type="text"
                            name="preco"
                            value={preco}
                            onChange={this.listennerInput}
                        />
                    </div>

                    <button type="button" onClick={this.submitForm} className="waves-effect waves-light btn indigo lighten-2">Salvar</button>
                </div>
            </form>
        )
    }
}
