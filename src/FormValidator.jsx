import React from 'react';
import validator from 'validator';

class FormValidator {

    constructor(validacoes) {
        this.validacoes = validacoes;
    }

    valida(state) {

        let validacao = this.objetoParaValidacao();
    
        this.validacoes.forEach(regra => {
            const campoValor = state[regra.campo.toString()];
            const args = regra.args  || [];
            // if ternário para estar preparado caso alguém passe o método direto sem ser string
            const metodoValidacao = typeof regra.metodo === 'string' ? validator[regra.metodo] : regra.metodo
    
            // se o retorno do metodo for diferente do esperado, validacao.isValid = false;
            if(metodoValidacao(campoValor, ...args, state) !== regra.validoQuando) {
                validacao[regra.campo] = {
                    isInvalid: true,
                    message: regra.mensagem
                }
                validacao.isValid = false;
            }
        });
    
        return validacao;
    }

    /** Cria objeto assumindo que todos os campos são válidos */
    objetoParaValidacao() {

        const validacao = {}
    
        this.validacoes.map(regra => (
            validacao[regra.campo] = {isInvalid: false, message: ''}
        ));
    
        return {isValid: true, ...validacao};

        /**
            Mapeia os objetos da instância do FormValidator para o tipo
            {   isValid: true,
                livro: {isInvalid: false, message: ""},
                nome: {isInvalid: false, message: ""},
                preco: {isInvalid: false, message: ""}
            }
         */
    } 
}

export default FormValidator;