import React, { Component, Fragment } from 'react'
import Header from './Header';
import DataTable from './DataTable';

class Autores extends Component {

    state = {
        autores: [
            {
                nome: 'Paulo',
                livro: 'React',
                preco: '1000'
            },
            {
                nome: 'Daniel',
                livro: 'Java',
                preco: '99'
            },
            {
                nome: 'Marcos',
                livro: 'Design',
                preco: '150'
            },
            {
                nome: 'Bruno',
                livro: 'DevOps',
                preco: '100'
            }
        ],
        titulo: 'Lista de Autores'

    }

    render() {
        return (
            <Fragment>
                <Header />
                <h3>Autores</h3>
                <div className="container mb-10">
                    <DataTable dados={this.state.autores} titulo={this.state.titulo} colunas={['nome']} />
                </div>
            </Fragment>
        )
    };
}
export default Autores;