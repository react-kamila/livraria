import React, { Fragment } from 'react'
import Header from './Header';
import 'materialize-css/dist/css/materialize.min.css';
import LinkWrapper from './LinkWrapper';
import book from './assets/images/books.png';

const NotFound = () => {
    return (
        <Fragment>
            <Header />
            <header className="not-found-header">
                <img src={book} className="not-found-logo" alt="books" />
                <h2>Ooops!</h2>
                <p>A página que você está procurando não existe.</p>
                <LinkWrapper to="/"  className="indigo lighten-1 btn">Página inicial</LinkWrapper>
            </header>
        </Fragment>
    );
}
export default NotFound;